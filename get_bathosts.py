import subprocess
import json
import requests
import re

of_interests = [
        "primary0",
        "bat0",
        "br-client",
        "br-wan"]

def find_mac_for_if(ifname, ip_l_output):
    match = False
    for l in ip_l_output.split("\n"):
        if match:
            return l.strip().split(" ")[1]
        if ifname in l:
            match = True
    return None

def to_slug(name, ifname):
    name = re.sub("[^a-zA-Z0-9-_]", "", name)
    return "{}-{}".format(name, ifname)

resp = requests.get("https://w.freifunk-bs.de/data/meshviewer.json")
if resp.status_code != 200:
    raise Exception("Unable to get current node list")

nodes = json.loads(resp.text)

results = {}
for node in nodes["nodes"]:
    results[node["hostname"]] = None
    for addr in node["addresses"]:
        if not "2001:bf7" in addr:
            continue
        print("{}: {}".format(node["hostname"], addr))
        p = subprocess.run(
                [
                    "ssh", 
                    "-o", "UserKnownHostsFile=/dev/null", 
                    "-o", "StrictHostKeyChecking=no", 
                    "-o", "ConnectTimeout=10", 
                    "root@{}".format(addr),
                    "ip l"
                    ], 
                capture_output=True
                )
        if p.returncode == 0:
            infos = {}
            infos["stdout"] = p.stdout.decode()
            infos["stderr"] = p.stderr.decode()
            infos["addr"] = addr
            infos["macs"] = { ifname: find_mac_for_if(ifname, infos["stdout"]) for ifname in of_interests }
            results[node["hostname"]] = infos
            break

old = []
for name, infos in results.items():
    if not infos:
        continue
    for ifname, mac in infos["macs"].items():
        if mac:
            if mac not in old:
                print("{} {}".format(mac, to_slug(name, ifname)))
                old.append(mac)
